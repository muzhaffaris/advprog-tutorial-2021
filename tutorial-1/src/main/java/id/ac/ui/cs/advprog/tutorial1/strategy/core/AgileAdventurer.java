package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {


    private String alias;

    public AgileAdventurer(){
        alias = "Agile Adventurer";
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    public String getAlias(){
        return alias;
    }

}
