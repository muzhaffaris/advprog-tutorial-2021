package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {


    public String defend(){
        return "Berat brader";
    }

    public String getType(){
        return "ARMOR";
    }
}
