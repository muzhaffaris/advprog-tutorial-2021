package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {


    private String alias;

    public MysticAdventurer(){
        alias = "Mystic Adventurer";
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    public String getAlias(){
        return alias;
    }
}
