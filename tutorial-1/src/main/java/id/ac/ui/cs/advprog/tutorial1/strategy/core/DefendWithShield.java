package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {


    public String defend(){
        return "Ciaatt Perisai!!";
    }

    public String getType(){
        return "SHIELD";
    }
}
