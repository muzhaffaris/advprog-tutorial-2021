package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me
    public void update(){
        if(guild.getQuestType().equals("Delivery") || guild.getQuestType().equals("Escort") || guild.getQuestType().equals("E") || guild.getQuestType().equals("D")){
            getQuests().add(guild.getQuest());
        }
    }
}
