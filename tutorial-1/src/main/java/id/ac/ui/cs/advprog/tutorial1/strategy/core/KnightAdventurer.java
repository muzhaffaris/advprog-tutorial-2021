package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {


    private String alias;

    public KnightAdventurer(){
        alias = "Knight Adventurer";
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    public String getAlias(){
        return alias;
    }
}
